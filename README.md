# README #

Minu töö tegemine sujus alguses hästi ja ladusalt. Esimeseks takistuseks sai panna elemente üksteisest eemale
hoidma. Proovisin kasutada collision detectionit ja siis panna elemendid üksteisega põrkuma, aga ei osanud seda
kuidagi enda tehtud koodiga ühendada. Samuti proovisin kasutada mouseover() funktsiooni, aga ei saanud veel tööle. 
Igatahes proovin seda koodi edasi teha ja lõpuks ka soovitud tulemuse saavutada.